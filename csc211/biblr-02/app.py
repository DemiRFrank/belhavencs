from flask import Flask

app = Flask(__name__)

# Route:
#   url: /
#   function: index
#   returns: index.html template
