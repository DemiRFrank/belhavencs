from flask import Flask, render_template

app = Flask(__name__)

# Route:
#   url: /api/v1/book/<book_id>
#   function: get_book
#   returns:
#     - 200: Dictionary of the form {'verses': [List of verse dictionaries]}
#     - 400 (Bad Request): Bad book ID

# Route:
#   url: /api/v1/chapter/<book_id>/<chapter_number>
#   function: get_chapter
#   returns:
#     - 200: Dictionary of the form {'verses': [List of verse dictionaries]}
#     - 400 (Bad Request): Bad book or chapter number

# Route:
#   url: /api/v1/chapter/<book_id>/<chapter_number>/<verse_number>
#   function: get_verse
#   returns:
#     - 200: Verse dictionary
#     - 400 (Bad Request): Bad book, chapter number, or verse number
